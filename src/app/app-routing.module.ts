import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {ViewIdeaComponent} from "./components/idea/view/view-idea.component";
import {AddIdeaComponent} from './components/idea/add-idea/add-idea.component';
import {IdeaSubmittedComponent} from './components/idea/idea-submitted/idea-submitted.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'idea/add',
    component: AddIdeaComponent
  },
  {
    path: 'idea/submitted',
    component: IdeaSubmittedComponent
  },
  {
    path: 'idea/:ideaId',
    component: ViewIdeaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
