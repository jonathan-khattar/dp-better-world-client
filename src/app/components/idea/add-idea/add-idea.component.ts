import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GOALS} from '../../../model/goal';
import {faChevronLeft, faImage} from '@fortawesome/free-solid-svg-icons';
import {IdeaService} from '../../../services/idea.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-add-idea',
  templateUrl: './add-idea.component.html',
  styleUrls: ['./add-idea.component.scss']
})
export class AddIdeaComponent implements OnInit {
  goals = GOALS;
  ideaForm: FormGroup;
  faChevronLeft = faChevronLeft;
  imageIcon = faImage;
  isSaving: boolean;
  submitHasError = false;
  errorMessage: string;

  constructor(private ideaService: IdeaService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.isSaving = false;
    this.ideaForm = this.formBuilder.group({
      publisherName: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      thumbnailUrl: ['', Validators.required],
      goal: ['', Validators.required],
    });
    this.errorMessage = 'There was an error submitting your idea';
  }

  submitForm(): void {
    this.isSaving = true;
    if (this.ideaForm.status !== 'INVALID') {
      this.ideaService.save(this.ideaForm.value).subscribe(res => {
        if (res.ok) {
          this.router.navigate(['/idea/submitted']);
        } else {
          this.submitHasError = true;
          this.errorMessage = 'There was an error saving your idea';
        }
      });
    }
    this.isSaving = false;
  }
}
