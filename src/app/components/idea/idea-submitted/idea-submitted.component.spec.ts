import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaSubmittedComponent } from './idea-submitted.component';

describe('IdeaSubmittedComponent', () => {
  let component: IdeaSubmittedComponent;
  let fixture: ComponentFixture<IdeaSubmittedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdeaSubmittedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaSubmittedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
