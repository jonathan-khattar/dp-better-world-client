import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-idea-submitted',
  templateUrl: './idea-submitted.component.html',
  styleUrls: ['./idea-submitted.component.scss']
})
export class IdeaSubmittedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
