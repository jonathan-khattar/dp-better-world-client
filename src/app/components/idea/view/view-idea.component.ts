import {Component, OnInit} from '@angular/core';
import {Idea} from '../../../model/idea';
import {IdeaService} from '../../../services/idea.service';
import {ActivatedRoute} from '@angular/router';
import {faChevronLeft, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {GOALS} from '../../../model/goal';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-view-idea',
  templateUrl: './view-idea.component.html',
  styleUrls: ['./view-idea.component.scss']
})
export class ViewIdeaComponent implements OnInit {

  idea: Idea;
  faChevronLeft = faChevronLeft;
  spinnerIcon = faSpinner;
  isMobile = window.innerWidth <= 375;
  isLoading: boolean;

  constructor(private ideaService: IdeaService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getIdea();

    window.addEventListener('resize', () => {
      this.isMobile = window.innerWidth <= 375;
    });
  }

  getIdea () {
    this.isLoading = true;
    this.route.params.subscribe(params => {
      this.ideaService.get(params.ideaId)
        .subscribe(idea => {
          this.idea = idea;
          this.idea.goal = GOALS.find(goal => goal.key === idea.goal).label;
          this.isLoading = false;
        });
    });
  }
}
