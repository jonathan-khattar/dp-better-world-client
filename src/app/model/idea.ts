export interface Idea {
  id: number;
  publisherName: string;
  title: string;
  goal: string;
  description: string;
  thumbnailUrl: string;
}
